const assert = require('assert');
const sinon = require('sinon');
const utils = require('../../../src/messageQueue/utils');

describe('[UNIT] Message Queue Utils', () => {
  let sandbox;

  beforeEach(() => {
    sandbox = sinon.createSandbox();
    sandbox.useFakeTimers();
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('Generate Metadata', () => {
    it('Should succeed : generate default metadata', () => {
      sandbox.clock.tick(100);

      const result = utils.generateMetadata();
      assert.deepEqual(result, {
        createdAt: 100,
        ttl: null,
        jammedAfter: 180000,
      });
    });

    it('Should succeed : override createdAt with enforced value', () => {
      sandbox.clock.tick(100);

      const result = utils.generateMetadata({ createdAt: 500 });
      assert.deepEqual(result, {
        createdAt: 100,
        ttl: null,
        jammedAfter: 180000,
      });
    });

    it('Should succeed : consider requested ttl and precalculate it', () => {
      sandbox.clock.tick(100);

      const result = utils.generateMetadata({ ttl: 500 });
      assert.deepEqual(result, {
        createdAt: 100,
        ttl: 600,
        jammedAfter: 180000,
      });
    });

    it('Should succeed : should keep any value requested that is not used internally', () => {
      sandbox.clock.tick(100);

      const result = utils.generateMetadata({ myOwnOption: 'myOwnValue' });
      assert.deepEqual(result, {
        createdAt: 100,
        ttl: null,
        jammedAfter: 180000,
        myOwnOption: 'myOwnValue',
      });
    });
  });

  describe('Is Expired', () => {
    it('Should succeed : return false if ttl is null', () => {
      sandbox.clock.tick(100);

      const result = utils.isExpired({ createdAt: 0, ttl: null });
      assert.equal(result, false);
    });

    it('Should succeed : return false if ttl is undefined', () => {
      sandbox.clock.tick(100);

      const result = utils.isExpired({ createdAt: 0 });
      assert.equal(result, false);
    });

    it('Should succeed : return false if current time < ttl', () => {
      sandbox.clock.tick(100);

      const result = utils.isExpired({ createdAt: 0, ttl: 120 });
      assert.equal(result, false);
    });

    it('Should succeed : return false if current time = ttl', () => {
      sandbox.clock.tick(100);

      const result = utils.isExpired({ createdAt: 0, ttl: 100 });
      assert.equal(result, false);
    });

    it('Should succeed : return true if current time > ttl', () => {
      sandbox.clock.tick(100);

      const result = utils.isExpired({ createdAt: 0, ttl: 80 });
      assert.equal(result, true);
    });
  });
});
