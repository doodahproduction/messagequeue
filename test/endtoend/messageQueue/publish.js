const assert = require('assert');
const sinon = require('sinon');
const IoRedis = require('ioredis');
const messageQueueFactory = require('../../../src/messageQueue');

describe('[END TO END] Redis', () => {
  describe('Publish', () => {
    let messageQueue;
    let ioRedisUsual;
    let sandbox;

    beforeEach(async () => {
      const settings = {
        port: 6379,
        host: '127.0.0.1',
      };

      messageQueue = await messageQueueFactory(settings);
      ioRedisUsual = new IoRedis(settings);

      sandbox = sinon.createSandbox();
      sandbox.useFakeTimers();
    });

    afterEach(async () => {
      await ioRedisUsual.del('{myChannel}:Pending');

      messageQueue.disconnect();
      ioRedisUsual.disconnect();

      sandbox.restore();
    });

    it('Publish a message, add metadata, insert it to the queue and listener receive notification', async () => {
      sandbox.clock.tick(100);

      const data = { data: 'myData' };
      await messageQueue.publish('myChannel', data);

      const [queueSize, entry] = await Promise.all([
        ioRedisUsual.llen('{myChannel}:Pending'),
        ioRedisUsual.lindex('{myChannel}:Pending', 0),
      ]);

      assert.equal(queueSize, 1);
      assert.deepEqual(JSON.parse(entry), {
        message: data,
        metadata: {
          createdAt: 100,
          ttl: null,
          jammedAfter: 180000,
        },
      });
    });

    it('Newly published insert at left of the queue', async () => {
      sandbox.clock.tick(100);

      const dataA = { data: 'dataA' };
      const dataB = { data: 'dataB' };
      await messageQueue.publish('myChannel', dataA);
      await messageQueue.publish('myChannel', dataB);

      const [queueSize, entry] = await Promise.all([
        ioRedisUsual.llen('{myChannel}:Pending'),
        ioRedisUsual.lindex('{myChannel}:Pending', 0),
      ]);

      assert.equal(queueSize, 2);
      assert.deepEqual(JSON.parse(entry), {
        message: dataB,
        metadata: {
          createdAt: 100,
          ttl: null,
          jammedAfter: 180000,
        },
      });
    });
  });
});
