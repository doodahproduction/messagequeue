const assert = require('assert');
const sinon = require('sinon');
const IoRedis = require('ioredis');
const moment = require('moment');
const _ = require('lodash');
const messageQueueFactory = require('../../../src/messageQueue');

describe('[END TO END] Redis', () => {
  describe('Subscribe', () => {
    let messageQueue;
    let ioRedis;

    beforeEach(async () => {
      const settings = {
        port: 6379,
        host: '127.0.0.1',
      };

      messageQueue = await messageQueueFactory(settings);
      ioRedis = new IoRedis(settings);
      return true;
    });

    afterEach(() => {
      return Promise.all([
        ioRedis.del('{myChannel}:Pending'),
        ioRedis.del('{myChannel}:Processing'),
        ioRedis.del('{myChannel}:Failed'),
        messageQueue.disconnect(),
        ioRedis.disconnect(),
      ]);
    });

    it('Channel unlistened, callback does not trigger', async () => {
      await messageQueue.subscribe(
        'myOtherChannel',
        () => { assert.fail('Should not receive ce message'); },
        1,
      );

      const data = { message: 1, metadata: { ttl: null } };
      await ioRedis.lpush('{myChannel}:Pending', JSON.stringify(data));
      await ioRedis.publish('myChannel', null);

      return new Promise(async (resolve) => {
        setTimeout(async () => {
          return resolve();
        }, 200);
      });
    });

    it('Channel listened, message expired, callback does not trigger, moved to fail', async () => {
      await messageQueue.subscribe(
        'myChannel',
        () => { assert.fail('Should not receive ce message'); },
        1,
      );

      const data = { message: 1, metadata: { createdAt: 0, ttl: 0 } };
      await ioRedis.lpush('{myChannel}:Pending', JSON.stringify(data));
      await ioRedis.publish('myChannel', null);

      return new Promise(async (resolve) => {
        setTimeout(async () => {
          const [pendingCount, processingCount, failedCount, failedData] = await Promise.all([
            ioRedis.llen('{myChannel}:Pending'),
            ioRedis.llen('{myChannel}:Processing'),
            ioRedis.llen('{myChannel}:Failed'),
            ioRedis.lindex('{myChannel}:Failed', 0),
          ]);

          assert.equal(pendingCount, 0);
          assert.equal(processingCount, 0);
          assert.equal(failedCount, 1);

          assert.deepEqual(JSON.parse(failedData), {
            ...data,
            reason: 'Message expired',
          });

          return resolve();
        }, 200);
      });
    });

    it('Channel listened, callback does trigger, moved to processing', async () => {
      const queueData = { message: 1, metadata: { createdAt: moment().valueOf(), ttl: 2000 } };

      const callbackSpy = sinon.spy((data, done, fail, next) => {
        assert.deepEqual(data, queueData);
        assert.ok(_.isFunction(done));
        assert.ok(_.isFunction(fail));
        assert.ok(_.isFunction(next));
      });

      await messageQueue.subscribe('myChannel', callbackSpy, 1);
      await ioRedis.lpush('{myChannel}:Pending', JSON.stringify(queueData));
      await ioRedis.publish('myChannel', null);

      return new Promise(async (resolve) => {
        setTimeout(async () => {
          const [pendingCount, processingCount, failedCount, processingData] = await Promise.all([
            ioRedis.llen('{myChannel}:Pending'),
            ioRedis.llen('{myChannel}:Processing'),
            ioRedis.llen('{myChannel}:Failed'),
            ioRedis.lindex('{myChannel}:Processing', 0),
          ]);

          assert.equal(pendingCount, 0);
          assert.equal(processingCount, 1);
          assert.equal(failedCount, 0);

          assert.deepEqual(JSON.parse(processingData), queueData);

          assert.equal(callbackSpy.callCount, 1);

          return resolve();
        }, 200);
      });
    });

    it('Remove from queue when done is called', async () => {
      const queueData = { message: 1, metadata: { createdAt: moment().valueOf(), ttl: 2000 } };

      const callback = (data, done) => {
        done();
      };

      await messageQueue.subscribe('myChannel', callback, 1);
      await ioRedis.lpush('{myChannel}:Pending', JSON.stringify(queueData));
      await ioRedis.publish('myChannel', null);

      return new Promise(async (resolve) => {
        setTimeout(async () => {
          const [pendingCount, processingCount, failedCount] = await Promise.all([
            ioRedis.llen('{myChannel}:Pending'),
            ioRedis.llen('{myChannel}:Processing'),
            ioRedis.llen('{myChannel}:Failed'),
          ]);

          assert.equal(pendingCount, 0);
          assert.equal(processingCount, 0);
          assert.equal(failedCount, 0);

          return resolve();
        }, 200);
      });
    });

    it('Move to failed queue when fail is called', async () => {
      const queueData = { message: 1, metadata: { createdAt: moment().valueOf(), ttl: 2000 } };

      const callback = (data, done, fail) => {
        fail('My valuable reason');
      };

      await messageQueue.subscribe('myChannel', callback, 1);
      await ioRedis.lpush('{myChannel}:Pending', JSON.stringify(queueData));
      await ioRedis.publish('myChannel', null);

      return new Promise(async (resolve) => {
        setTimeout(async () => {
          const [pendingCount, processingCount, failedCount, failedData] = await Promise.all([
            ioRedis.llen('{myChannel}:Pending'),
            ioRedis.llen('{myChannel}:Processing'),
            ioRedis.llen('{myChannel}:Failed'),
            ioRedis.lindex('{myChannel}:Failed', 0),
          ]);

          assert.equal(pendingCount, 0);
          assert.equal(processingCount, 0);
          assert.equal(failedCount, 1);

          assert.deepEqual(JSON.parse(failedData), {
            ...queueData,
            reason: 'My valuable reason',
          });

          return resolve();
        }, 200);
      });
    });

    it('Process only one message if next not called and concurrency is 1', async () => {
      const queueDataA = { message: 1, metadata: { createdAt: moment().valueOf(), ttl: 2000 } };
      const queueDataB = { message: 2, metadata: { createdAt: moment().valueOf(), ttl: 2000 } };

      const callbackSpy = sinon.spy(() => {});

      await messageQueue.subscribe('myChannel', callbackSpy, 1);
      await ioRedis.lpush('{myChannel}:Pending', JSON.stringify(queueDataA));
      await ioRedis.lpush('{myChannel}:Pending', JSON.stringify(queueDataB));
      await ioRedis.publish('myChannel', null);

      return new Promise(async (resolve) => {
        setTimeout(async () => {
          assert.equal(callbackSpy.callCount, 1);
          return resolve();
        }, 200);
      });
    });

    it('Process messages up to concurrency', async () => {
      const queueDataA = { message: 1, metadata: { createdAt: moment().valueOf(), ttl: 2000 } };
      const queueDataB = { message: 2, metadata: { createdAt: moment().valueOf(), ttl: 2000 } };
      const queueDataC = { message: 3, metadata: { createdAt: moment().valueOf(), ttl: 2000 } };

      const callbackSpy = sinon.spy(() => {});

      await messageQueue.subscribe('myChannel', callbackSpy, 2);
      await ioRedis.lpush('{myChannel}:Pending', JSON.stringify(queueDataA));
      await ioRedis.lpush('{myChannel}:Pending', JSON.stringify(queueDataB));
      await ioRedis.lpush('{myChannel}:Pending', JSON.stringify(queueDataC));
      await ioRedis.publish('myChannel', null);

      return new Promise(async (resolve) => {
        setTimeout(async () => {
          assert.equal(callbackSpy.callCount, 2);
          return resolve();
        }, 200);
      });
    });

    it('Process messages up to available list', async () => {
      const queueDataA = { message: 1, metadata: { createdAt: moment().valueOf(), ttl: 2000 } };
      const queueDataB = { message: 2, metadata: { createdAt: moment().valueOf(), ttl: 2000 } };
      const queueDataC = { message: 3, metadata: { createdAt: moment().valueOf(), ttl: 2000 } };

      const callbackSpy = sinon.spy(() => {});

      await messageQueue.subscribe('myChannel', callbackSpy, 5);
      await ioRedis.lpush('{myChannel}:Pending', JSON.stringify(queueDataA));
      await ioRedis.lpush('{myChannel}:Pending', JSON.stringify(queueDataB));
      await ioRedis.lpush('{myChannel}:Pending', JSON.stringify(queueDataC));
      await ioRedis.publish('myChannel', null);

      return new Promise(async (resolve) => {
        setTimeout(async () => {
          assert.equal(callbackSpy.callCount, 3);
          return resolve();
        }, 200);
      });
    });

    it('Process another message when next is called', async () => {
      const queueDataA = { message: 1, metadata: { createdAt: moment().valueOf(), ttl: 2000 } };
      const queueDataB = { message: 2, metadata: { createdAt: moment().valueOf(), ttl: 2000 } };

      const callbackSpy = sinon.spy((data, done, fail, next) => {
        done();
        next();
      });

      await messageQueue.subscribe('myChannel', callbackSpy, 1);
      await ioRedis.lpush('{myChannel}:Pending', JSON.stringify(queueDataA));
      await ioRedis.lpush('{myChannel}:Pending', JSON.stringify(queueDataB));
      await ioRedis.publish('myChannel', null);

      return new Promise(async (resolve) => {
        setTimeout(async () => {
          assert.equal(callbackSpy.callCount, 2);
          return resolve();
        }, 200);
      });
    });

    it('Process no other message when next is called but no more messages', async () => {
      const queueData = { message: 1, metadata: { createdAt: moment().valueOf(), ttl: 2000 } };

      const callbackSpy = sinon.spy((data, done, fail, next) => {
        done();
        next();
      });

      await messageQueue.subscribe('myChannel', callbackSpy, 1);
      await ioRedis.lpush('{myChannel}:Pending', JSON.stringify(queueData));
      await ioRedis.publish('myChannel', null);

      return new Promise(async (resolve) => {
        setTimeout(async () => {
          assert.equal(callbackSpy.callCount, 1);
          return resolve();
        }, 200);
      });
    });
  });
});
