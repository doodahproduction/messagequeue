const assert = require('assert');
const IoRedis = require('ioredis');
const moment = require('moment');
const messageQueueFactory = require('../../../src/messageQueue');

describe('[END TO END] Message Queue', () => {
  describe('CheckJammed', () => {
    let messageQueue;
    let ioRedis;

    beforeEach(async () => {
      const settings = {
        port: 6379,
        host: '127.0.0.1',
      };

      messageQueue = await messageQueueFactory(settings);
      ioRedis = new IoRedis(settings);
      return true;
    });

    afterEach(() => {
      return Promise.all([
        ioRedis.del('{myChannel}:Pending'),
        ioRedis.del('{myChannel}:Processing'),
        ioRedis.del('{myChannel}:Failed'),
        ioRedis.del('{myChannel}:Score'),
        messageQueue.disconnect(),
        ioRedis.disconnect(),
      ]);
    });

    it('Do nothing if score and processing are empty', async () => {
      const interval = await messageQueue.checkJammed('myChannel', 50);

      return new Promise(async (resolve) => {
        setTimeout(async () => {
          clearInterval(interval);
          assert.ok(true);

          return resolve();
        }, 80);
      });
    });

    it('Do not move to failed as not considered jammed yet', async () => {
      const createdAt = moment().valueOf();
      const queueData = { message: 1, metadata: { createdAt, jammedAfter: 200 } };

      await Promise.all([
        ioRedis.zadd('{myChannel}:Score', createdAt, JSON.stringify(queueData)),
        ioRedis.lpush('{myChannel}:Processing', JSON.stringify(queueData)),
      ]);

      const interval = await messageQueue.checkJammed('myChannel', 50);

      return new Promise(async (resolve) => {
        setTimeout(async () => {
          clearInterval(interval);

          const [processingCount, failedCount] = await Promise.all([
            ioRedis.llen('{myChannel}:Processing'),
            ioRedis.llen('{myChannel}:Failed'),
          ]);

          assert.equal(processingCount, 1);
          assert.equal(failedCount, 0);

          return resolve();
        }, 80);
      });
    });

    it('Multiples jammed process, but consider only one at a time, oldest first', async () => {
      const createdAt = moment().valueOf();
      const queueDataA = { message: 1, metadata: { createdAt, jammedAfter: 0 } };
      const queueDataB = { message: 2, metadata: { createdAt, jammedAfter: 0 } };

      await Promise.all([
        ioRedis.zadd('{myChannel}:Score', createdAt, JSON.stringify(queueDataA)),
        ioRedis.lpush('{myChannel}:Processing', JSON.stringify(queueDataA)),
      ]);

      await Promise.all([
        ioRedis.zadd('{myChannel}:Score', createdAt, JSON.stringify(queueDataB)),
        ioRedis.lpush('{myChannel}:Processing', JSON.stringify(queueDataB)),
      ]);

      const interval = await messageQueue.checkJammed('myChannel', 100);

      return new Promise(async (resolve) => {
        setTimeout(async () => {
          clearInterval(interval);

          const [processingCount, failedCount, failedData] = await Promise.all([
            ioRedis.llen('{myChannel}:Processing'),
            ioRedis.llen('{myChannel}:Failed'),
            ioRedis.lindex('{myChannel}:Failed', 0),
          ]);

          assert.equal(processingCount, 1);
          assert.equal(failedCount, 1);

          assert.deepEqual(JSON.parse(failedData), {
            ...queueDataA,
            reason: 'Message jammed in process after 0 ms',
          });

          return resolve();
        }, 180);
      });
    });

    it('Treat all jammed process after enough time', async () => {
      const createdAt = moment().valueOf();
      const queueDataA = { message: 1, metadata: { createdAt, jammedAfter: 0 } };
      const queueDataB = { message: 2, metadata: { createdAt, jammedAfter: 0 } };

      await Promise.all([
        ioRedis.zadd('{myChannel}:Score', createdAt, JSON.stringify(queueDataA)),
        ioRedis.lpush('{myChannel}:Processing', JSON.stringify(queueDataA)),
      ]);

      await Promise.all([
        ioRedis.zadd('{myChannel}:Score', createdAt, JSON.stringify(queueDataB)),
        ioRedis.lpush('{myChannel}:Processing', JSON.stringify(queueDataB)),
      ]);

      const interval = await messageQueue.checkJammed('myChannel', 50);

      return new Promise(async (resolve) => {
        setTimeout(async () => {
          clearInterval(interval);

          const [processingCount, failedCount, failedDataA, failedDataB] = await Promise.all([
            ioRedis.llen('{myChannel}:Processing'),
            ioRedis.llen('{myChannel}:Failed'),
            ioRedis.lindex('{myChannel}:Failed', 0),
            ioRedis.lindex('{myChannel}:Failed', 1),
          ]);

          assert.equal(processingCount, 0);
          assert.equal(failedCount, 2);

          assert.deepEqual(JSON.parse(failedDataA), {
            ...queueDataA,
            reason: 'Message jammed in process after 0 ms',
          });


          assert.deepEqual(JSON.parse(failedDataB), {
            ...queueDataB,
            reason: 'Message jammed in process after 0 ms',
          });

          return resolve();
        }, 180);
      });
    });
  });
});
