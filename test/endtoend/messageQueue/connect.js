const assert = require('assert');
const moment = require('moment');
const messageQueueFactory = require('../../../src/messageQueue');

describe('[END TO END] Redis', () => {
  describe('Connect', () => {
    let messageQueue;

    afterEach(() => {
      if (messageQueue) {
        messageQueue.disconnect();
      }
    });

    it('Should fail : cannot connect to redis', async () => {
      const settings = {
        port: 100,
        host: '127.0.0.1',
      };

      messageQueue = await messageQueueFactory(settings)
        .then(() => {
          assert.fail('Should fail');
        }, (e) => {
          assert.deepEqual(e, new Error('Redis could not connect after 1 attempts'));
        });
    });

    it('Should fail : cannot connect to redis after many attemps with small delay', async () => {
      const settings = {
        port: 100,
        host: '127.0.0.1',
        retryCount: 10,
        retryDelay: 300,
      };

      messageQueue = await messageQueueFactory(settings)
        .then(() => {
          assert.fail('Should fail');
        }, (e) => {
          assert.deepEqual(e, new Error('Redis could not connect after 10 attempts'));
        });
    }).timeout(3100);

    it('Should fail : cannot connect to redis after two attemps with long delay', async () => {
      const settings = {
        port: 100,
        host: '127.0.0.1',
        retryCount: 2,
        retryDelay: 3000,
      };

      const startTime = moment().valueOf();
      messageQueue = await messageQueueFactory(settings)
        .then(() => {
          assert.fail('Should fail');
        }, (e) => {
          const endTime = moment().valueOf();

          assert.ok(endTime >= startTime + 3000);
          assert.deepEqual(e, new Error('Redis could not connect after 2 attempts'));
        });
    }).timeout(6100);

    it('Should success : connect to redis', async () => {
      const settings = {
        port: 6379,
        host: '127.0.0.1',
      };

      messageQueue = await messageQueueFactory(settings);
      assert.ok(true);
    });
  });
});
