const messageQueueFactory = require('./messageQueue');

module.exports = async () => {
  if (!process.env.redisSettings) {
    process.env.redisSettings = JSON.stringify({
      port: 6379,
      host: '127.0.0.1',
    });
  }

  const messageQueue = await messageQueueFactory(JSON.parse(process.env.redisSettings));

  return messageQueue;
};
