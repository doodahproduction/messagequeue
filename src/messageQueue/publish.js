const Promise = require('bluebird');

module.exports = (ioRedis, utils) => {
  /**
   * @describe Publish message to target queue.
   * Listeners get notified that a new message juste entered.
   * @param {string} channel Channel name
   * @param {object} message Message as JSON bject
   * @param {object} options See utils.generateMetadata for available options
   * @return {Promise} Promise
   */
  return async (channel, message, options = {}) => {
    return Promise.try(async () => {
      const metadata = utils.generateMetadata(options);
      const data = JSON.stringify({ metadata, message });

      await ioRedis.lpush(utils.getQueueNameForPending(channel), data);
      ioRedis.publish(channel, JSON.stringify({ event: 'NEW', message: 'New message in queue' }));
    });
  };
};
