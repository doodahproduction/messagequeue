const Promise = require('bluebird');

module.exports = (ioRedis) => {
  /**
   * @describe Observe queues length.
   * @param {string} channel Channel name.
   * @return {object} Length of each related queue.
   */
  return async (channel) => {
    const [pending, processing, failed] = await Promise.all([
      ioRedis.llen(`{${channel}}:Pending`),
      ioRedis.llen(`{${channel}}:Processing`),
      ioRedis.llen(`{${channel}}:Failed`),
    ]);

    return { pending, processing, failed };
  };
};
