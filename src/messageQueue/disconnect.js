const Promise = require('bluebird');

module.exports = (redisList) => {
  return async () => {
    return Promise.map(redisList, (redis) => {
      return new Promise((resolve, reject) => {
        const timeout = setTimeout(() => {
          return reject(new Error('Redis disconnect timeout'));
        }, 10000);

        redis.on('close', () => {
          clearTimeout(timeout);
          return resolve();
        });

        redis.disconnect();
      });
    });
  };
};
