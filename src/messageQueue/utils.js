const _ = require('lodash');
const moment = require('moment');

/**
 * @description Return queue name for pending messages
 * @param {string} channel Channel name
 * @return {string} Queue name
 */
exports.getQueueNameForPending = (channel) => {
  return `{${channel}}:Pending`;
};

/**
 * @description Return queue name for processing messages
 * @param {string} channel Channel name
 * @return {string} Queue name
 */
exports.getQueueNameForProcessing = (channel) => {
  return `{${channel}}:Processing`;
};

/**
 * @description Return queue name for failed messages
 * @param {string} channel Channel name
 * @return {string} Queue name
 */
exports.getQueueNameForFailed = (channel) => {
  return `{${channel}}:Failed`;
};

/**
 * @description Return score list name
 * @param {string} channel Channel name
 * @return {string} Score list name
 */
exports.getQueueNameForScore = (channel) => {
  return `{${channel}}:Score`;
};

/**
* @describe Generate metadata associated
* @param {object} options Requested options
* @param {uint} options.ttl Time to live, expressed in miliseconds
* @return {object} Metadata
*/
exports.generateMetadata = (options) => {
  const timestamp = moment().valueOf();

  const defaultOptions = {
    ttl: null,
  };

  const { ttl } = _.merge({}, defaultOptions, options);

  const overridables = {
    ttl: ttl ? timestamp + ttl : null,
  };

  const enforced = {
    createdAt: timestamp,
    jammedAfter: 180000,
  };

  return _.merge({}, options, overridables, enforced);
};

/**
* @describe Check if message is expired
* @param {object} metadata Metadata
* @return {boolean}
*/
exports.isExpired = (metadata) => {
  const { ttl, createdAt } = metadata;
  const timestamp = moment().valueOf();

  if (_.isNull(ttl) || _.isUndefined(ttl)) {
    return false;
  }

  return (timestamp > createdAt + ttl);
};

/**
* @describe Check if message is jammed in processing
* @param {uint} startedTimestamp Timestamp at wich processing started
* @param {object} metadata Metadata
* @return {boolean}
*/
exports.isJammed = (startedTimestamp, metadata) => {
  const { jammedAfter } = metadata;
  const timestamp = moment().valueOf();

  return (timestamp > startedTimestamp + jammedAfter);
};
