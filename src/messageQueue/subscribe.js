/* eslint no-param-reassign: 0 no-use-before-define: 0 */
const moment = require('moment');

module.exports = (ioRedisSubscribe, ioRedisUsual, utils, state) => {
  const nextFactory = (channel, callback, maxConcurrent) => {
    return () => {
      processNextMessage(channel, callback, maxConcurrent);
    };
  };

  const doneFactory = (channel, data) => {
    return async () => {
      state.count -= 1;

      await Promise.all([
        ioRedisUsual.lrem(utils.getQueueNameForProcessing(channel), -1, data),
        ioRedisUsual.zrem(utils.getQueueNameForScore(channel), data),
      ]);
    };
  };

  const failFactory = (channel, data) => {
    return async (reason) => {
      state.count -= 1;

      const { message, metadata } = JSON.parse(data);
      const newData = JSON.stringify({ message, metadata, reason });

      await Promise.all([
        ioRedisUsual.rpush(utils.getQueueNameForFailed(channel), newData),
        ioRedisUsual.lrem(utils.getQueueNameForProcessing(channel), -1, data),
        ioRedisUsual.zrem(utils.getQueueNameForScore(channel), data),
      ]);
    };
  };

  /**
   * @description Process next message and trigger callback if respect conditions
   * @param {string} channel Channel name
   * @param {function} callback Function to call message received
   * @param {uint} maxConcurrent Max number of concurrent messages
   */
  const processNextMessage = async (channel, callback, maxConcurrent = 1) => {
    if (state.count >= maxConcurrent) {
      return;
    }

    const data = await ioRedisUsual.rpoplpush(
      utils.getQueueNameForPending(channel),
      utils.getQueueNameForProcessing(channel),
    );

    await ioRedisUsual.zadd(utils.getQueueNameForScore(channel), moment().valueOf(), data);

    if (!data) {
      return;
    }

    state.count += 1;
    const { message, metadata } = JSON.parse(data);

    const fail = failFactory(channel, data);
    const done = doneFactory(channel, data);
    const next = nextFactory(channel, callback, maxConcurrent);

    if (utils.isExpired(metadata)) {
      await fail('Message expired');
      await next();

      return;
    }

    callback({ message, metadata }, done, fail, next);
    processNextMessage(channel, callback, maxConcurrent);
  };

  /**
   * @describe Subscribe to a channel to get notified if a new message is created.
   * @param {string} channel Channel name
   * @param {function} callback Function to call message received
   * @param {uint} maxConcurrent Max number of concurrent messages
   * @return {Promise} Promise
   */
  return async (channel, callback, maxConcurrent = 1) => {
    await ioRedisSubscribe.subscribe(channel);

    ioRedisSubscribe.on('message', (fromChannel) => {
      if (fromChannel === channel) {
        processNextMessage(channel, callback, maxConcurrent);
      }
    });
  };
};
