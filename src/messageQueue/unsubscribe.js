/* eslint no-param-reassign: 0 no-use-before-define: 0 */

module.exports = (ioRedisSubscribe) => {
  /**
   * @describe Unsubscribe from a channel
   * @param {string} channel Channel name
   * @return {Promise} Promise
   */
  return async (channel) => {
    await ioRedisSubscribe.unsubscribe(channel);
  };
};
