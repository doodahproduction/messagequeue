const Promise = require('bluebird');
const _ = require('lodash');

module.exports = (IoRedis, settings) => {
  return () => {
    return new Promise((resolve, reject) => {
      const { retryCount, retryDelay } = settings;
      const withRetryStrategy = _.merge(
        {},
        settings,
        {
          retryStrategy: (times) => {
            if (times < retryCount) {
              return retryDelay;
            }

            return reject(new Error(`Redis could not connect after ${retryCount || 1} attempts`));
          },
        },
      );

      const ioRedis = new IoRedis(withRetryStrategy);

      if (ioRedis.status === 'connected') {
        return resolve(ioRedis);
      }

      ioRedis.on('ready', () => {
        return resolve(ioRedis);
      });

      ioRedis.on('error', (error) => {
        if (error.code !== 'ECONNREFUSED') {
          return reject(new Error(`Redis failed : ${error.message}`));
        }
      });
    });
  };
};
