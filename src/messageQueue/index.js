const IoRedis = require('ioredis');

const connectFactory = require('./connect');
const disconnectFactory = require('./disconnect');
const publishFactory = require('./publish');
const subscribeFactory = require('./subscribe');
const unsubscribeFactory = require('./unsubscribe');
const observeFactory = require('./observe');
const checkJammedFactory = require('./checkJammed');

const utils = require('./utils');
const { generateState } = require('./state');

module.exports = async (settings) => {
  const [publisher, subscriber, usual] = await Promise.all([
    connectFactory(IoRedis, settings)(),
    connectFactory(IoRedis, settings)(),
    connectFactory(IoRedis, settings)(),
  ]);

  const disconnect = disconnectFactory([publisher, subscriber, usual]);
  const publish = publishFactory(publisher, utils);
  const observe = observeFactory(usual);
  const checkJammed = checkJammedFactory(usual, utils);

  const state = generateState();

  const subscribe = subscribeFactory(subscriber, usual, utils, state);
  const unsubscribe = unsubscribeFactory(subscriber);

  return {
    disconnect,
    publish,
    subscribe,
    unsubscribe,
    observe,
    checkJammed,
  };
};
