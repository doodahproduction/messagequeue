module.exports = (ioRedisUsual, utils) => {
  return (channel, interval) => {
    return setInterval(async () => {
      const result = await ioRedisUsual.zrange(`{${channel}}:Score`, 0, 0, 'WITHSCORES');

      if (!result.length) {
        return;
      }

      const [data, score] = result;
      const { message, metadata } = JSON.parse(data);

      if (utils.isJammed(parseInt(score, 10), metadata)) {
        const newData = JSON.stringify({ message, metadata, reason: `Message jammed in process after ${metadata.jammedAfter} ms` });

        await Promise.all([
          ioRedisUsual.lrem(`{${channel}}:Processing`, -1, data),
          ioRedisUsual.rpush(`{${channel}}:Failed`, newData),
          ioRedisUsual.zrem(`{${channel}}:Score`, data),
        ]);
      }
    }, interval);
  };
};
