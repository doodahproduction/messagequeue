module.exports = {
  "extends": "airbnb-base",
  "env": {
    "mocha": true,
  },
  "rules": {
    "mocha/no-exclusive-tests": 0,
    "arrow-body-style": 0,
    "object-curly-newline": 0,
    "global-require": 0,
    "import/no-dynamic-require": 0,
    "import/no-extraneous-dependencies": 0,
    "prefer-destructuring": 0
  }
};