const messageQueueFactory = require('../src');

const start = async () => {
  const { observe } = await messageQueueFactory();

  setInterval(async () => {
    const { pending, processing, failed } = await observe('order');
    console.log(`[Pending] : ${pending} | [Processing] : ${processing} | [Failed] : ${failed}`);
  }, 100);
};

start();
