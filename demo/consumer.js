const messageQueueFactory = require('../src');

const start = async () => {
  const { subscribe, unsubscribe, checkJammed } = await messageQueueFactory();

  const callback = (data, done, fail, next) => {
    console.log('[CONSUME] Consuming message : ', data);

    // We simulate some process on this side by setting a timeout of 300ms
    setTimeout(async () => {
      await done();
      next();
    }, 300);
  };

  await subscribe('order', callback, 5);

  const jammedInterval = checkJammed('order', 10000);

  process.on('SIGTERM', async () => {
    // We stop the interval
    clearInterval(jammedInterval);

    // We unsubscribe from queue
    await unsubscribe('order');

    // Give some time to end started processes
    setTimeout(() => {
      console.log('Gracefully shut down');
      process.exit(0);
    }, 2000);
  });
};

start();
