const moment = require('moment');
const messageQueueFactory = require('../src');

const start = async () => {
  const { publish } = await messageQueueFactory();

  setInterval(async () => {
    const data = { id: moment().valueOf() };
    await publish('order', data);
    console.log('[PUBLISH] New message : ', JSON.stringify(data));
  }, 10);
};

start();
